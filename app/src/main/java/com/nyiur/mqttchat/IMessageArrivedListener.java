package com.nyiur.mqttchat;

/**
 * Created by 9bits on 6/24/15.
 */
public interface IMessageArrivedListener {

    void messageArrived(String message);

}
